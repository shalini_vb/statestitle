import os
import sys
import json
from models import start, CountryFruit
import datetime

# Assuming that the file that comes in will always be named fruitdata.txt
FILENAME = "fruitdata.txt"

class FruitPalService(object):
    """
    Class representing the trading service
    """
    def __init__(self,today, session):
        """

        :param today: date to store for today's data
        :param session: db session for storing all data
        """
        self._session = session
        if os.path.exists(FILENAME):
            self._date_to_check = today
            self.file_to_db()
        else:
            country_fruit = session.query(CountryFruit).order_by(CountryFruit.ctime.desc()).first()
            self._date_to_check = country_fruit.ctime

    def file_to_db(self):
        """
         If a file was found store today's trade values into db for easy access
         and back up data file.
        """
        try:
            with open(FILENAME, 'r') as f:
                data = json.load(f)
        except Exception as e:
            print(e)
            print("Couldn't read in today's data: Aborting")
            exit()

        for element in data:
            country_fruit = CountryFruit()
            country_fruit.commodity = element['COMMODITY']
            country_fruit.country = element['COUNTRY']
            country_fruit.fixed = element['FIXED_OVERHEAD']
            country_fruit.variable = element['VARIABLE_OVERHEAD']
            country_fruit.ctime = self._date_to_check
            self._session.add(country_fruit)
        self._session.commit()
        file_backup = FILENAME+str(self._date_to_check)
        os.rename(FILENAME,file_backup)

    def get_trade(self, fruit_name, cost_per_ton, number_of_tons):
        """
         Query for desired list of countries for given fruit and latest date
         and return the calculated trade values as a dictionary
        """
        country_fruit_list = self._session.query(CountryFruit) \
                                    .filter(CountryFruit.commodity == fruit_name) \
                                    .filter(CountryFruit.ctime == self._date_to_check) \
                                    .all()
        result = {}
        for row in country_fruit_list:
            cost_variable = cost_per_ton + row.variable
            total = (cost_variable * number_of_tons) + row.fixed
            result[total] = {"Cost_Variable": cost_variable,
                             "Country": row.country,
                             "Fixed": row.fixed}
        return result

    @staticmethod
    def is_fruit( fruit):
        """
        Simple test to validate given commodity is actual fruit.
        """
        valid_fruits = ["mango", "apple", "grape", "banana", "strawberry", "orange"]
        if fruit.lower() in valid_fruits:
            return True
        else:
            return False

def main():
    if len(sys.argv) != 4:
        print("Usage: fruitpal commodity_name, cost_per_ton, number_of_ton")
        exit()
    session = start()
    today = datetime.datetime.today()
    # Initialize Service object
    fruit_pal = FruitPalService(today, session)

    # Check that commodity is an actual fruit
    if FruitPalService.is_fruit(sys.argv[1]):
        fruit_name = sys.argv[1].lower()
    else:
        print('Commodity {} is not valid'.format(sys.argv[1]))
        exit()

    # Check that the values entered are numbers
    try:
        cost_per_ton = float(sys.argv[2])
        number_of_tons = int(sys.argv[3])
    except ValueError:
        print('Both cost_per_ton = "{}" and number_of_ton = "{}" should be numbers.'.
              format(sys.argv[2], sys.argv[3]) )
        exit()

    # Get trade values for all countries
    result_list = fruit_pal.get_trade( fruit_name, cost_per_ton, number_of_tons)

    # Display in required format by sorting keys of
    for elem in sorted(result_list.items(),reverse=True):
        element = "< {} {:.2f} | ({:.2f}*{})+{}".format(elem[1]["Country"], elem[0],
                                                elem[1]["Cost_Variable"],
                                                number_of_tons,
                                                elem[1]["Fixed"])
        print(element)
    session.close()


if __name__ == '__main__':
        main()