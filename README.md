# statestitle
Assumption : 
1. The data file will always be available in the directory of the running script
2. The data file will be named “fruitdata.txt”

This would be my first iteration with this project with many things still not complete.

Still TODO:
1. Unit tests 
2. Proper validation of commodity.
3. How to update the list of valid fruits?
4. Proper commenting.

The next iteration I would probably make this a REST service with either Mysql or PostgreSQL db
If we were to move this into AWS, on data file being uploaded into S3 a lambda could kick off processing of that file.
